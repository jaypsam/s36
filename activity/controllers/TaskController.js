// To connect via local location
const Task = require('../models/Task.js');

// Creating a function
// module.exports.getAllTasks = () => {
// 	// business logic

// 	return Task.find({}).then(result => {
// 		return result
// 	})
// };


module.exports.getAllTasks = (taskId) => {
	// business logic

	return Task.findById(taskId).then(result => {
		return result
	})
};

module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error){
			console.log(error)
			return false
			return 'Duplicate Task Found'
		} else {
			return savedTask // 'Task.created succesfully'
		}
	})
};

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			result.status = newContent.status

			return result.save().then((updatedTask, error) => {
				if(error) {
					console.log(error)
					return false
				} else {
					return updatedTask
				}
			})
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
		if(error){
			console.log(error)
			return false
		} else {
			return 'Data has been deleted' //deletedTask
		}
	})
}